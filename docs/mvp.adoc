== MVP

Pour commencer, nous avons réalisé un MVP (Minimum Viable Product) avec les fonctionnalités suivantes:

- Une unique chat room 
- Pas de persistance des données 
- L'application est actuellement séparée en deux projets :

*Front:* Simple application React qui affiche une page de dialogue et un champ d'input   

*Back:* Application Scala utilisant le toolkit Akka.
