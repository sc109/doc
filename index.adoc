= Chat Scala
Julian LABATUT, Kalil PELISSIER, Malo POLESE
:encoding: utf-8
:lang: en
:toc: preamble
:sectnums:
:partnums:
:numbered:
:chapter-signifier: 
:hardbreaks-option:
:icons: font

== Contexte

URL DEMO (http://oggy.cluster-2020-6.dopolytech.fr)

L'objectif de ce https://docs.google.com/document/d/1anV0G7cVZiEljeEf_qi-5mF_ErKitjxJkAWrq0JjRXY/edit#heading=h.fylfh8oec611[projet] est de réaliser une application de chat. L'application offre la possibilité aux utilisateurs de créer des salons de discussion dans lesquels ils peuvent discuter librement. Les fonctionnalités sont les mêmes pour les salons de deux utilisateurs et les salons à plus de deux utilisateurs.

Le chat doit être hautement résilient et disponible. Tout crash ou redémarrage du serveur ne doit pas entrainer de perte de données ou une panne visible dans le frontend.

L'application devra permettre aux utilisateurs de créer des messages et des rappels différés. Dit autrement, ces messages seront envoyés aux utilisateurs après un délai spécifié.

include::./docs/fonctionnement.adoc[]

include::./docs/architecture.adoc[]

include::./docs/recap.adoc[]